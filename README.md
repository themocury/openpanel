# dp_flink
数据中台 flink

## preface
本项目主要环绕三个概念: ***proto, flow, job***。 
- proto 数据的协议，包括数据的描述以及介质的实体命名。（ex: 写入db中的某张表）
- flow 单个业务的数据流，输入输出皆需遵循proto协议。
- job 承载多个数据流的任务。
开发流程建议先定义输入输出的proto, 再根据proto开发数据流，最后将数据流加上相应的业务所属任务中。
## 开发手册
### requirements
- java 8
- gradle 6.5+
- git
- docker (advised)

### proto & flow & job
开发时重点关注三者的档案放置**路径**以及**命名**。通用组件大多会由命名规则生成对应的名称，以达到减少配置项和规则统一的目的。
#### position
flow, job 需放在对应的区域package下 (hk, us, sg...etc)，如下所示  
```
#example hk地区ods层的quote_client任务 有Permission, Subscription 等数据流  

main.com.futunn  
└─ hk                                     (area package)
│   └─ ods                                (layer package)  
│   │   └─ quote.client                   (job package)    
│   │   │         └─ flow                 (flow package)  
│   │   │         │  └─ Permission.java   (flow class)
│   │   │         │  └─ Subscription.java (flow class)
│   │   │         └─── Job.java           (job class)
│   │   └─ xxx
│   └─ dws 
└─ us
```
proto文件放置在proto文件夹下，根据area, layer放置对应的文件夹。
```
//example ods层的Permission, Subscription表
src.main.proto
          └─ hk
          │   └─ ods                                   (layer direcory)      
          │   │   └─ OdsQuoteClientPermission.proto    (protobuf)  
          │   │   └─ OdsQuoteClientSubscription.proto  (protobuf)
          │   └─ dws
          │       └─ xxx.proto
          └─ us
```

#### protobuf
命名为 **${layer}${job}${instance}**, 驼峰形式。
java package 须为 **com.futunn.proto.${area}.${layer}.${jobPackage}** 来保证 source sinker命名规则正确解析，为了保证介质的实体唯一（例如db的某张表与proto 一一对应）。
```proto
syntax = "proto2";
// layer=ods, jobPackage=quote.client
package hk.ods.quote.client.permission;
option java_package = "com.futunn.proto.hk.ods.quote.client";  // important!

// ...
```

#### flow
命名没硬性规定，尽量贴近业务，驼峰形式。需要 implement Flow interface 实作 registerProto, build 两个方法。
- public Iterable<Class<? extends GeneratedMessageV3>> registerProto()  
注册flow用到的protobuf文件到sEnv上 避免集群计算时，类型消除造成序列化的相关问题。
- public void build(StreamExecutionEnvironment sEnv)  
数据流的operator规划：例如 source、mapper、sinker。
```java
// example 
public class Permission implements Flow {
    @Override
    public Iterable<Class<? extends GeneratedMessageV3>> registerProto() {
        return Arrays.asList(RawQuoteClientPermission.Record.class, OdsQuoteClientPermission.Record.class);
    }

    @Override
    public void build(StreamExecutionEnvironment sEnv) throws Exception {
        var kafkaSource = createKafkaSource(this, RawQuoteClientPermission.Record.class, SourceFactory.DeserializerType.AGENT);
        var kafkaSink = createKafkaSink(OdsQuoteClientPermission.Record.class);
        var hdfsSink = createHdfsSink(OdsQuoteClientPermission.Record.class, record -> record.getData().getLoginTime() * 1000, SinkFactory.HdfsTimeZone.AmericaNewYork);

        var ods = sEnv.addSource(kafkaSource).name(setKafkaName(RawQuoteClientPermission.Record.class))
                .map(new Raw2Ods(OdsQuoteClientPermission.Record.class));

        ods.addSink(kafkaSink).name(setKafkaName(OdsQuoteClientPermission.Record.class));
        ods.addSink(mysqlSink).name(setMysqlName(OdsQuoteClientPermission.Record.class));
    }
}
```

#### job
命名恒为**Job.java**, 需要 extends FlowRunner。内有addFlow, run两个父方法。
- public static void addFlow(Flow... flows)  
将 flow 注册到 job's env
- public static void run(FlowRunner flowRunner)  
运行任务，传入FlowRunner实体是为了自动命名。
```java
//example
public class Job extends FlowRunner {
    public static void main(String[] args) throws Exception {
        env.enableCheckpointing(300000);
        addFlow(
                new Permission(),
                new Subscription()
        );
        run(new Job());
    }
}
```

### source, sinker
所有source, sinker皆根据proto的名称以及proto's java package配合规则来命名，保证一致性。
#### source
下面以 jobPackage=quote.client protoFile=OdsQuoteClientPermission.proto 为例
- kafka
    - topic: ${layer}_${type}_${job}_${pb} ex: etl_db_quote_client_permission
    - group id: ${source_layer}_bda_flink_${target_layer}_${job}_${flow} ex: raw_bda_flink_ods_quote_client_permission
#### sinker
- kafka
    - pb 名称与 kafka topic 一一对应。
    - topic: ${layer}_pb_${job}_${pb} ex: etl_pb_quote_client_permission
- mysql
    - table: ${layer}.${job}_${pb} ex: ods.quote_client_permission
- hdfs
    - directory: ${job}_${pb} ex: quote_client_permission
- hbase
    - table: ${job}_${pb} ex: quote_client_permission
    
### notice
#### AGG Layer (聚合层) ： 消息收集任务以及聚合计算任务 
聚合层是较为特殊的数据层，将不同地区(hk, us...)的数据打通。命名方式也需要特别注意。  
当dws的任务需要将数据sink到聚合层，则要sink的pb需放在 **proto.global.dws层下**  
而聚合层的任务接收global.dws层的数据后，计算并根据需要sink到当前或再度sink到global网域。  
该pb需放在 **proto.hk(us..)/global.agg** 代表该数据是由agg的任务生成。  

```
#ex: dws统计各地区的开户状态，收集到hk分部进行聚合计算，再将结果让各地区收集。
#分析: 只在香港做聚合分析（保证统计结果唯一），有两次收集行为。
dws's job:  hk/us/sg.ods.pb -> global.dws.pb
agg's job: 
    - hk collector job: global.dws.pb -> global.dws.pb 收集各路数据到hk
    - hk aggregate job: global.dws.pb -> global.agg.pb 进行聚合计算
    - hk collector job: global.agg.pb -> global.agg.pb 各地区(us, sg)再拉聚合计算结果
```
 
## 开发流程
### 本地测试
src/main/resources/version.properties 选项决定运行的环境以及地区 (只与本地调试有关 类似maven Project 不影响正式环境制品包生成)
```properties
# dev/prod
env=dev
# hk/us/sg
area=us
```
建议第一次运行执行 gradle wrapper 来保证以 gradle 6.5版本运行。 避免插件运行失败。
```bash
> gradle wrapper
```

下列为常用的gradle指令
```bash
# 运行所有tasks
> ./gradlew build

# 运行所有tasks *skip tests
> ./gradlew assemble

# compile protofile 
> ./gradlew generateProto

# remove build dir and clean protofile
> ./gradlew clean

# could combine two order
> ./gradlew clean assemble
```
gradle assemble 完成后 jar file 位于 build/libs/dp_flink_*.jar  
可利用 docker-compose.yml 建立本地 flink cluster 来模拟打包运行的状态。需修改 --job-classname
```bash
# 启用
docker-compose up
# 关闭
docker-compose down
```
### 版本控制
#### 命名规则
- 本地个人分支: dev-${area}-${user} ex: dev-hk-adrian
- 测试环境分支: dev-${area} ex: dev-hk  
**dev-hk/dev-us/dev-sg** 为特殊分支会触发CI 生成jar包放置测试集群。  
*请注意:测试集群以及本地开发集群为不同概念 测试环境一般需要给其他开发者联调使用，须保证相对稳定*
- 主分支: master
触发CI 会生成多个地区的正式环境jar包 可在fpub对应的地区包执行流水线拉取对应的制品包。 ex: dp_flink_us 执行流水线，自动生成版本为 prod-us 的jar包
#### 合并流程
本地个人分支 -merge-> 测试环境分支 -merge-> 主分支  
执行合并前皆须保证前一个状态的运行结果稳定。
